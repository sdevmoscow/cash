﻿using Cash.DataContext;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Atol.Drivers10.Fptr;
using System.Threading;
using Helpers;
using Cash.SmsContext;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Reflection;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;

namespace Cash
{
    public class PaymentData
    {
        public string PhoneNumber { get; set; }
        public string ClientFio { get; set; }
        public double Value { get; set; }
    }

    public class PaymentRegister
    {
        MobileBilling4Context ctx = new MobileBilling4Context();
        SMSSERVERContext ctxSms = new SMSSERVERContext();
        Cash cash = null;
        private int Receipts = 0;
        private int Errors = 0;
        private double Summa = 0;
        private const int StopHour = 23;
        private const int StopMinute = 55;
        private bool noPrint = false;
        public PaymentRegister(string[] args)
        {
            string email = string.Empty;
            noPrint = args.FirstOrDefault(x => x.ToLower()=="-noprint") != null;
            for (int i=0; i<args.Length; i++)
            {
                if (args[i].ToLower() == "-e" && i < args.Length-1)
                {
                    email = args[i + 1];
                    break;
                }
            }

            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fileVersionInfo.ProductVersion;
            Helpers.Helper.Log("I", "Запуск программы");
            Helpers.Helper.Log("I", "Версия программы: " + version);


            cash = new Cash(email);
            cash.Open();
            CheckOpen();
            CashStatus status = cash.ShortStatus();
        }

        public void CheckOpen()
        {
            if (!cash.CheckOpen())
            {
                Helpers.Helper.Log("E", "ВНИМАНИЕ! КАССА НЕ ОТКРЫТА!");
            }
        }


        public ReceiptStatus Register(PaymentData payment)
        {
            string text = payment.PhoneNumber + " " + payment.Value.ToString("0.00") + " " + payment.ClientFio;
            if (cash.CheckOpen())
            { 
                //CashStatus status = cash.ShortStatus();
                //cash.OperatorCash.Helpers.Helper.Login("Сис.администратор", string.Empty); - делается в чеке
                // открытие смены - необязательно (откроется с первым чеком)
                // cash.OpenShift();
                // Открытие печатного чека
                cash.OpenReceipt(Constants.LIBFPTR_RT_SELL, "Сис.администратор", string.Empty, payment.ClientFio, payment.PhoneNumber, electronically: noPrint);
                // Регистрация товара
                cash.Registration("Оплата услуг связи", payment.Value, 1, Constants.LIBFPTR_TAX_NO);
                // Оплата чека
                cash.Payment(payment.Value, Constants.LIBFPTR_PT_CASH);
                // Регистрация налога на чек
                cash.Tax(0, Constants.LIBFPTR_TAX_NO);
                // Регистрация итога чека
                cash.ReceiptTotal(payment.Value);
                // Закрытие полностью оплаченного чека
                CloseReceiptStatus closeStatus = cash.CloseReceipt();
                ReceiptStatus status = cash.QueryData();
                Receipts++;
                Summa += payment.Value;
                Helpers.Helper.Log("I", "Метод Register: оплата зарегистрирована в ОФД: " + text);
                return status;
            }
            else
            {
                Errors++;
                Helpers.Helper.Log("E", "ВНИМАНИЕ! КАССА НЕ ОТКРЫТА! " + text);
                return new ReceiptStatus();
            }
        }

        public void Run()
        {
            //test SendReport();
            while (true)
            {
                DateTime date = DateTime.Now;
                if (date.Hour == StopHour && date.Minute >= StopMinute)
                {
                    // временное окно для закрытия смены
                    CloseShift();
                }
                else
                {
                    // временное окно для регистрации оплат
                    CheckPayment();
                }

                Thread.Sleep(2000);
            }
        }

        private void SendReport()
        {
            CashStatus status = cash.ShortStatus();
            
            string text = "Отчет о закрытии смены." + Environment.NewLine +
                "Чеков: " + Receipts.ToString() + Environment.NewLine +
                "Сумма: " + Summa.ToString("F2") + " руб." + Environment.NewLine +
                "Ошибок: " + Errors.ToString() + Environment.NewLine +
                "Наличие бумаги: " + (status.isPaperPresent ? "Да" : "Нет") + Environment.NewLine +
                "Мало бумаги: " + (status.isPaperNearEnd ? "Да" : "Нет");

            string html = "<h4>Отчет о закрытии смены.</h4>"+
                "<table>" +
                "<tr><td>Чеков:</td><td>" + Receipts.ToString() + "</td></tr>" +
                "<tr><td>Сумма:</td><td>" + Summa.ToString("F2") + " руб." + "</td></tr>" +
                "<tr><td>Ошибок:</td><td>" + Errors.ToString() + "</td></tr>" +
                "<tr><td>Наличие бумаги:</td><td>" + (status.isPaperPresent ? "Да" : "Нет") + "</td></tr>" +
                "<tr><td>Мало бумаги:</td><td>" + (status.isPaperNearEnd ? "Да" : "Нет") + "</td></tr>" +
                "</table>";

            //MailServer server = new MailServer
            //{
            //    RobotEmail = "service.tti.com@gmail.com",
            //    SmtpHost = "smtp.gmail.com",
            //    SmtpPassword = "RGBhu89)",
            //    SmtpPort = 587
            //};

            MailServer server = new MailServer
            {
                RobotEmail = "billing@optimtele.com",
                SmtpHost = "smtp.yandex.ru",
                SmtpPassword = "Billing123>",
                SmtpPort = 587
            };

            MailLetter letter = new MailLetter
            {
                EmailAdresat = "chief@itec.ru", // "sdevmoscow@gmail.com",
                EmailBody = html,
                EmailSubject = "Отчет о закрытии смены"
            };

            try
            {
                MailHelper.SendMessage(server, letter);
            }
            catch(Exception x)
            {
                Helpers.Helper.Log("E", "Ошибка при отправке email о закрытии смены: " + x.Message);
            }

            try 
            { 
                ctxSms.MessageOut.Add(new MessageOut
                {
                    MessageTo = "79672133000",
                    MessageText = text,
                    Gateway = "goip1",
                    Priority = 0
                });
                ctxSms.SaveChanges();
            }
            catch (Exception x)
            {
                Helpers.Helper.Log("E", "Ошибка при отправке СМС о закрытии смены: " + x.Message);
            }

            /*sqlReqwest = "INSERT INTO [SMSSERVER].[dbo].[MessageOut] ([MessageTo], [MessageText], [Gateway], [Priority])" + " VALUES ('7" + num + "', '" + text + "', 'goip1', '0'" + ")";*/

            int a = 0;
        }
        private void CloseShift()
        {
            if (cash.CheckOpen())
            {
                try
                {
                    cash.CloseShift();
                    Helpers.Helper.Log("I", "Смена закрыта");
                    SendReport();
                    Receipts = 0;
                    Errors = 0;
                    DateTime date = DateTime.Now;
                    while (date.Hour == StopHour && date.Minute >= StopMinute)
                    {
                        Thread.Sleep(100);
                        date = DateTime.Now;
                    }
                    Helpers.Helper.Log("I", "Возобновление регистрации платежей");
                }
                catch (Exception x)
                {
                    Helpers.Helper.Log("E", "Ошибка при закрытии смены: " + x.Message);
                }
            }
            else
            {
                Helpers.Helper.Log("E", "Метод CloseShift: Касса не открыта");
            }
        }

        public void CheckPayment()
        {
            Payment payment = null;
            try
            {
                payment = ctx.Payment.Where(x => x.PaymentTypeIdPaymentType == 6 && x.Ofd == false).OrderBy(x => x.IdPayment).FirstOrDefault();
            }
            catch (Exception x)
            {
                Helpers.Helper.Log("E", "Метод Register: ошибка чтения базы данных: " + x.Message);
            }

            if (payment != null)
            {
                var clientPhoneNumber = ctx.ClientPhoneNumber.FirstOrDefault(x => x.IdPhoneNumber == payment.ClientPhoneNumberIdPhoneNumber);
                if (clientPhoneNumber != null)
                {
                    var client = ctx.Client.FirstOrDefault(x => x.IdClient == clientPhoneNumber.ClientIdClient);
                    if (client != null)
                    {
                        var paymentData = new PaymentData()
                        {
                            ClientFio = Helpers.Helper.GetClientFIO(client),
                            PhoneNumber = "+7" + clientPhoneNumber.PhoneNumber,
                            Value = payment.Value //!!!!
                            //ClientFio = "Тестовый Т.А.", 
                            //PhoneNumber = "89167220074",
                            //Value = payment.Value / 1000
                        };

                        var result = Register(paymentData);
                        var text = paymentData.PhoneNumber + " " + paymentData.Value.ToString("0.00") + " " + paymentData.ClientFio;

                        if (result.success == true)
                        {
                            try
                            {
                                payment.Ofd = true;
                                payment.Receipt = JsonSerializer.Serialize(result);
                                ctx.SaveChanges();
                                Helpers.Helper.Log("I", "Метод Register: сделана отметка о регистрации оплаты в базе данных: " + text);
                            }
                            catch (Exception x)
                            {
                                Helpers.Helper.Log("E", "Метод Register: оплата зарегистрирована в ОФД, ошибка при регистрации факта оплаты в базе данных: " + text);
                            }
                        }
                        else
                        {
                            Helpers.Helper.Log("E", "Метод Register: оплата не зарегистрирована в ОФД");
                        }
                    }
                }
            }
        }
    }
}
