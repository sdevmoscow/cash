﻿using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace Helpers
{
    public class MailLetter
    {
        public string EmailAdresat { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public List<string> AttachFiles { get; set; }
        public List<string> AttachImages { get; set; }
    }

    public class MailServer
    {
        public string RobotEmail { get; set; }
        public string SmtpHost { get; set; }
        public string SmtpPassword { get; set; }
        public int SmtpPort { get; set; }
    }

    public class MailHelper
    {
        public static void SendMessage(MailServer server, MailLetter letter)
        {
            MailMessage mailMsg = new MailMessage();
            mailMsg.From = new MailAddress(server.RobotEmail);

            if (letter.AttachImages != null && letter.AttachImages.Count > 0)
            {
                AlternateView avHtml =
                    AlternateView.CreateAlternateViewFromString(letter.EmailBody, null, System.Net.Mime.MediaTypeNames.Text.Html);

                int z = 0;
                foreach (string img in letter.AttachImages)
                {
                    if (System.IO.File.Exists(img))
                    {
                        LinkedResource ImgLogo = new LinkedResource(img, System.Net.Mime.MediaTypeNames.Image.Jpeg) { ContentId = "pic" + z };
                        avHtml.LinkedResources.Add(ImgLogo);
                    }
                    z++;
                }
                //LinkedResource ImgLogo = new LinkedResource(absolutPath + "../Graphics/mailLogo.png") { ContentId = "logo" }; //cid:logo
                //avHtml.LinkedResources.Add(ImgLogo);

                //LinkedResource ImgFacebook = new LinkedResource(absolutPath + "../Graphics/mailFacebook.png") { ContentId = "face" };   //cid:face
                //avHtml.LinkedResources.Add(ImgFacebook);

                //LinkedResource ImgTwit = new LinkedResource(absolutPath + "../Graphics/mailTweet.png") { ContentId = "tweet" };      //cid:tweet
                //avHtml.LinkedResources.Add(ImgTwit);

                mailMsg.AlternateViews.Add(avHtml);
            }

            mailMsg.To.Add(letter.EmailAdresat);
            mailMsg.Subject = letter.EmailSubject;
            mailMsg.IsBodyHtml = true;

            mailMsg.Body = letter.EmailBody;
            SmtpClient smtp = new SmtpClient();

            smtp.Host = server.SmtpHost;
            smtp.Port = server.SmtpPort;

            if (letter.AttachFiles != null)
            {
                int i = 1;
                foreach (string file in letter.AttachFiles)
                {
                    if (System.IO.File.Exists(file))
                    {
                        Attachment data = new Attachment(file, System.Net.Mime.MediaTypeNames.Application.Octet);

                        System.Net.Mime.ContentDisposition disposition = data.ContentDisposition;
                        disposition.CreationDate = System.IO.File.GetCreationTime(file);
                        disposition.FileName = String.Format("Download{0:000}{1}", i, System.IO.Path.GetExtension(file));
                        disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
                        disposition.ReadDate = System.IO.File.GetLastAccessTime(file);
                        mailMsg.Attachments.Add(data);
                    }
                }
            }

            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential(server.RobotEmail, server.SmtpPassword);

            smtp.EnableSsl = true;
            smtp.Send(mailMsg);
        }
    }
}
