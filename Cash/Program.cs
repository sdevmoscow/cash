﻿using System;

namespace Cash
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Программа регистрации платежей через Сбербанк в онлайн-кассе");
            PaymentRegister paymentRegister = new PaymentRegister(args);
            paymentRegister.Run();
        }
    }
}
