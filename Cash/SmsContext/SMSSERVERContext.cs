﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace Cash.SmsContext
{
    public partial class SMSSERVERContext : DbContext
    {
        public SMSSERVERContext()
        {
        }

        public SMSSERVERContext(DbContextOptions<SMSSERVERContext> options)
            : base(options)
        {
        }

        public virtual DbSet<MessageIn> MessageIn { get; set; }
        public virtual DbSet<MessageLog> MessageLog { get; set; }
        public virtual DbSet<MessageOut> MessageOut { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            /*if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=95.211.15.131\\SQLEXPRESS;Database=SMSSERVER;user id=bill;password=bill");
            }*/
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json")
                            .Build();
                var connectionString = configuration["SmsServerConnectionString"];
                Helpers.Helper.Log("I", "Connection string: " + connectionString);
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MessageIn>(entity =>
            {
                entity.Property(e => e.Gateway).HasMaxLength(80);

                entity.Property(e => e.MessageFrom).HasMaxLength(80);

                entity.Property(e => e.MessagePdu).HasColumnName("MessagePDU");

                entity.Property(e => e.MessageTo).HasMaxLength(80);

                entity.Property(e => e.MessageType).HasMaxLength(80);

                entity.Property(e => e.ReceiveTime).HasColumnType("datetime");

                entity.Property(e => e.SendTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Smsc)
                    .HasColumnName("SMSC")
                    .HasMaxLength(80);

                entity.Property(e => e.UserId).HasMaxLength(80);
            });

            modelBuilder.Entity<MessageLog>(entity =>
            {
                entity.HasIndex(e => new { e.MessageId, e.SendTime })
                    .HasName("IDX_MessageId");

                entity.Property(e => e.ErrorCode).HasMaxLength(80);

                entity.Property(e => e.ErrorText).HasMaxLength(80);

                entity.Property(e => e.Gateway).HasMaxLength(80);

                entity.Property(e => e.MessageFrom).HasMaxLength(80);

                entity.Property(e => e.MessageId).HasMaxLength(80);

                entity.Property(e => e.MessagePdu).HasColumnName("MessagePDU");

                entity.Property(e => e.MessageTo).HasMaxLength(80);

                entity.Property(e => e.MessageType).HasMaxLength(80);

                entity.Property(e => e.ReceiveTime).HasColumnType("datetime");

                entity.Property(e => e.SendTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.StatusText).HasMaxLength(80);

                entity.Property(e => e.UserId).HasMaxLength(80);
            });

            modelBuilder.Entity<MessageOut>(entity =>
            {
                entity.HasIndex(e => e.IsRead)
                    .HasName("IDX_IsRead");

                entity.Property(e => e.Gateway).HasMaxLength(80);

                entity.Property(e => e.MessageFrom).HasMaxLength(80);

                entity.Property(e => e.MessageTo).HasMaxLength(80);

                entity.Property(e => e.MessageType).HasMaxLength(80);

                entity.Property(e => e.Scheduled).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasMaxLength(80);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
