﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Atol.Drivers10.Fptr;

namespace Cash
{
    public enum CloseReceiptStatus
    {
        CLOSED = 0,
        NOT_CLOSED = 1
    }

    public class CashStatus
    {
        public bool isCashDrawerOpened = false;
        public bool isPaperPresent = false;
        public bool isPaperNearEnd = false;
        public bool isCoverOpened = false;
    }

    public class ReceiptStatus
    {
        public uint documentNumber = 0;
        public uint receiptType = 0;
        public double receiptSum = 0;
        public String fiscalSign = String.Empty;
        public DateTime dateTime = new DateTime();
        public bool success = false;
    }

    public class Cash
    {
        private IFptr fptr = null;
        private string version;
        private bool isOpened;
        private string emailForReceipt;
        public Cash(string email)
        {
            Init();
            emailForReceipt = email;
        }
    
        public void Init()
        {
            if (fptr == null)
            {
                // Инициализация драйвера
                fptr = new Fptr();
                // Получение версии
                version = fptr.version();
            }
        }

        public bool Open()
        {
            Init();
            // Установка соединения с ККТ
            fptr.open();
            // Проверка состояния логического соединения
            isOpened = fptr.isOpened();
            return isOpened;
        }

        public bool CheckOpen()
        {
            Init();
            isOpened = fptr.isOpened();
            // Установка соединения с ККТ
            if (!isOpened)
            {
                fptr.open();
                isOpened = fptr.isOpened();
            }
            // Проверка состояния логического соединения
            return isOpened;
        }

        public void Close()
        {
            fptr.close();
            isOpened = fptr.isOpened();
            // Деинициализация драйвера
            fptr.destroy();
            fptr = null;
        }

        public CashStatus ShortStatus()
        {
            // Короткий запрос статуса ККТ
            fptr.setParam(Constants.LIBFPTR_PARAM_DATA_TYPE, Constants.LIBFPTR_DT_SHORT_STATUS);
            fptr.queryData();

            return new CashStatus
            {
                isCashDrawerOpened = fptr.getParamBool(Constants.LIBFPTR_PARAM_CASHDRAWER_OPENED),
                isPaperPresent = fptr.getParamBool(Constants.LIBFPTR_PARAM_RECEIPT_PAPER_PRESENT),
                isPaperNearEnd = fptr.getParamBool(Constants.LIBFPTR_PARAM_PAPER_NEAR_END),
                isCoverOpened = fptr.getParamBool(Constants.LIBFPTR_PARAM_COVER_OPENED)
            };
        }


        // регистрация оператора
        public void OperatorLogin(string oper, string inn)
        {
            // Открытие печатного чека
            fptr.setParam(1021, oper);
            if (!String.IsNullOrWhiteSpace(inn))
            {
                fptr.setParam(1203, inn); // ИНН оператора -необязательно ?
            }
            fptr.operatorLogin();
        }

        // открытие смены - необязательно (откроется с первым чеком)
        public void OpenShift()
        {
            fptr.openShift();
            fptr.checkDocumentClosed();
        }

        // закрытие смены
        public void CloseShift()
        {
            fptr.setParam(Constants.LIBFPTR_PARAM_REPORT_TYPE, Constants.LIBFPTR_RT_CLOSE_SHIFT);
            fptr.report();
            // Проверка закрытия документа
            fptr.checkDocumentClosed();
        }


        private void SetParam(int paramCode, string paramValue)
        {
            if (!String.IsNullOrWhiteSpace(paramValue))
            {
                fptr.setParam(paramCode, paramValue);
            }
        }

        // Открытие печатного чека
        public void OpenReceipt(int type, string operater, string operaterInn, string customer, string phone, bool electronically)
        {
            /* Тип чека (LIBFPTR_PARAM_RECEIPT_TYPE) может принимать следующие значения:
            LIBFPTR_RT_SELL - чек прихода (продажи)
            LIBFPTR_RT_SELL_RETURN - чек возврата прихода (продажи)
            LIBFPTR_RT_SELL_CORRECTION - чек коррекции прихода
            LIBFPTR_RT_SELL_RETURN_CORRECTION - чек коррекции возврата прихода
            LIBFPTR_RT_BUY - чек расхода (покупки)
            LIBFPTR_RT_BUY_RETURN - чек возврата расхода (покупки)
            LIBFPTR_RT_BUY_CORRECTION - чек коррекции расхода
            LIBFPTR_RT_BUY_RETURN_CORRECTION - чек коррекции возврата расхода
            Чтобы чек не печатался (электронный чек), нужно установить параметру LIBFPTR_PARAM_RECEIPT_ELECTRONICALLY значение true. */

            // Открытие печатного чека
            SetParam(1021, operater);
            SetParam(1203, operaterInn);
            fptr.operatorLogin();
            SetParam(1227, customer);
            SetParam(1008, String.IsNullOrEmpty(emailForReceipt) ? phone : emailForReceipt);
            fptr.setParam(Constants.LIBFPTR_PARAM_RECEIPT_TYPE, type);
            if (electronically)
                fptr.setParam(Constants.LIBFPTR_PARAM_RECEIPT_ELECTRONICALLY, true);
            fptr.openReceipt();
        }

        // Регистрация товара
        public void Registration(string productName, double price, int quantity, int taxType)
        {
            // Регистрация позиции без указания суммы налога
            fptr.setParam(Constants.LIBFPTR_PARAM_COMMODITY_NAME, productName);
            fptr.setParam(Constants.LIBFPTR_PARAM_PRICE, price);
            fptr.setParam(Constants.LIBFPTR_PARAM_QUANTITY, quantity);
            fptr.setParam(Constants.LIBFPTR_PARAM_TAX_TYPE, taxType);  //Constants.LIBFPTR_TAX_NO
            fptr.registration();
        }

        // Оплата чека
        public void Payment(double summ, int paymentType)
        {
            fptr.setParam(Constants.LIBFPTR_PARAM_PAYMENT_TYPE, paymentType); //Constants.LIBFPTR_PT_CASH
            fptr.setParam(Constants.LIBFPTR_PARAM_PAYMENT_SUM, summ);
            fptr.payment();
        }

        // Регистрация налога на чек
        public void Tax(double summ, int taxType)
        {
            fptr.setParam(Constants.LIBFPTR_PARAM_TAX_TYPE, taxType); //Constants.LIBFPTR_TAX_NO
            fptr.setParam(Constants.LIBFPTR_PARAM_TAX_SUM, summ);
            fptr.receiptTax();
        }

        // Регистрация итога чека
        public void ReceiptTotal(double summ)
        {
            fptr.setParam(Constants.LIBFPTR_PARAM_SUM, summ);
            fptr.receiptTotal();
        }

        // Закрытие полностью оплаченного чека
        public ReceiptStatus QueryData()
        {
            fptr.setParam(Constants.LIBFPTR_PARAM_FN_DATA_TYPE, Constants.LIBFPTR_FNDT_LAST_RECEIPT);
            fptr.fnQueryData();

            return new ReceiptStatus
            {
                documentNumber = fptr.getParamInt(Constants.LIBFPTR_PARAM_DOCUMENT_NUMBER),
                receiptType = fptr.getParamInt(Constants.LIBFPTR_PARAM_RECEIPT_TYPE),
                receiptSum = fptr.getParamDouble(Constants.LIBFPTR_PARAM_RECEIPT_SUM),
                fiscalSign = fptr.getParamString(Constants.LIBFPTR_PARAM_FISCAL_SIGN),
                dateTime = fptr.getParamDateTime(Constants.LIBFPTR_PARAM_DATE_TIME),
                success = true
            };
        }

        // Закрытие полностью оплаченного чека
        public CloseReceiptStatus CloseReceipt()
        {
            fptr.closeReceipt();

            // Проверка закрытия документа (на примере закрытия фискального чека)
            while (fptr.checkDocumentClosed() < 0)
            {
                // Не удалось проверить состояние документа. Вывести пользователю текст ошибки, попросить устранить неполадку и повторить запрос
                //Console.WriteLine(fptr.errorDescription());
                Thread.Sleep(200);
                continue;
            }

            Thread.Sleep(100);

            if (!fptr.getParamBool(Constants.LIBFPTR_PARAM_DOCUMENT_CLOSED))
            {
                // Документ не закрылся. Требуется его отменить (если это чек) и сформировать заново
                fptr.cancelReceipt();
                return CloseReceiptStatus.NOT_CLOSED;
            }

            if (!fptr.getParamBool(Constants.LIBFPTR_PARAM_DOCUMENT_PRINTED))
            {
                // Можно сразу вызвать метод допечатывания документа, он завершится с ошибкой, если это невозможно
                while (fptr.continuePrint() < 0)
                {
                    // Если не удалось допечатать документ - показать пользователю ошибку и попробовать еще раз.
                    //Console.WriteLine(String.Format("Не удалось напечатать документ (Ошибка \"{0}\"). Устраните неполадку и повторите.", fptr.errorDescription()));
                    Thread.Sleep(200);
                    continue;
                }
            }

            // Допечатывание документа
            fptr.continuePrint();
            return CloseReceiptStatus.CLOSED;
        }

        // Регистрация итога чека
        public void CheckOFD()
        {
            // Диагностика соединения с ОФД
            fptr.setParam(Constants.LIBFPTR_PARAM_REPORT_TYPE, Constants.LIBFPTR_RT_OFD_TEST);
            fptr.report();
        }

    }
}

