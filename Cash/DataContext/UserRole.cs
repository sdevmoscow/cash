﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class UserRole
    {
        public UserRole()
        {
            Task = new HashSet<Task>();
            User = new HashSet<User>();
        }

        public int IdUserRole { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Task> Task { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
