﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class TariffTariffOption
    {
        public int TariffIdTariff { get; set; }
        public int TariffOptionIdTariffOption { get; set; }

        public virtual Tariff TariffIdTariffNavigation { get; set; }
        public virtual TariffOption TariffOptionIdTariffOptionNavigation { get; set; }
    }
}
