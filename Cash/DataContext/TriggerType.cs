﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class TriggerType
    {
        public TriggerType()
        {
            Trigger = new HashSet<Trigger>();
        }

        public int IdTriggerType { get; set; }
        public string Name { get; set; }
        public string Pattern { get; set; }

        public virtual ICollection<Trigger> Trigger { get; set; }
    }
}
