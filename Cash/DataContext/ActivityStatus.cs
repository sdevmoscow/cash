﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class ActivityStatus
    {
        public ActivityStatus()
        {
            ClientPhoneNumber = new HashSet<ClientPhoneNumber>();
        }

        public int IdActivityStatus { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ClientPhoneNumber> ClientPhoneNumber { get; set; }
    }
}
