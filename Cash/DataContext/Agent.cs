﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Agent
    {
        public Agent()
        {
            ClientPhoneNumber = new HashSet<ClientPhoneNumber>();
            Tariff = new HashSet<Tariff>();
        }

        public int IdAgent { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public string Rate { get; set; }
        public string Comment { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public virtual ICollection<ClientPhoneNumber> ClientPhoneNumber { get; set; }
        public virtual ICollection<Tariff> Tariff { get; set; }
    }
}
