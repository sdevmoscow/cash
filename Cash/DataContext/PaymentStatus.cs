﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class PaymentStatus
    {
        public PaymentStatus()
        {
            Payment = new HashSet<Payment>();
        }

        public int IdPaymentStatus { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Payment> Payment { get; set; }
    }
}
