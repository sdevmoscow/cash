﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class PriorityType
    {
        public PriorityType()
        {
            Task = new HashSet<Task>();
            TaskByMessage = new HashSet<TaskByMessage>();
        }

        public int IdPriorityType { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Task> Task { get; set; }
        public virtual ICollection<TaskByMessage> TaskByMessage { get; set; }
    }
}
