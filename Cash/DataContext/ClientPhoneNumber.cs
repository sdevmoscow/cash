﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class ClientPhoneNumber
    {
        public ClientPhoneNumber()
        {
            Accrual = new HashSet<Accrual>();
            AccrualDetailed = new HashSet<AccrualDetailed>();
            ClientPhoneNumberConnectedToTariffOption = new HashSet<ClientPhoneNumberConnectedToTariffOption>();
            Correct = new HashSet<Correct>();
            Details = new HashSet<Details>();
            Discount = new HashSet<Discount>();
            Invoice = new HashSet<Invoice>();
            Payment = new HashSet<Payment>();
            PaymentDistributed = new HashSet<PaymentDistributed>();
            Task = new HashSet<Task>();
        }

        public string PhoneNumber { get; set; }
        public double? BalanseCur { get; set; }
        public double? BalansePrev { get; set; }
        public double? Delta { get; set; }
        public DateTime? DateActivateFirst { get; set; }
        public DateTime? DateBlock { get; set; }
        public string Info { get; set; }
        public int IdPhoneNumber { get; set; }
        public double? LastAccrual { get; set; }
        public double? Oc { get; set; }
        public double? Rc { get; set; }
        public double? FeeWithOptions { get; set; }
        public double? Uc { get; set; }
        public bool? IsNonCash { get; set; }
        public bool? IsIntranet { get; set; }
        public int? ActivityStatusIdActivityStatus { get; set; }
        public int? AddStatusIdAddStatus { get; set; }
        public int? AgentIdAgent { get; set; }
        public int? ClientIdClient { get; set; }
        public double? DiscountIdDiscount { get; set; }
        public int? ProfileGnkIdProfileGnk { get; set; }
        public int? TariffIdTariff { get; set; }
        public int? FrontIdTariff { get; set; }
        public int? CreditDepth { get; set; }
        public int? DataLimit { get; set; }
        public DateTime? Dtup { get; set; }
        public DateTime? DateChangePp { get; set; }
        public DateTime? Dtblk { get; set; }
        public int? Dtstat { get; set; }
        public int? Hlr { get; set; }
        public string Iccid { get; set; }
        public bool? IsDonor { get; set; }
        public DateTime? Dtinfdop { get; set; }
        public DateTime? Dtinfabon { get; set; }
        public string Pass { get; set; }
        public DateTime? DateRebil { get; set; }
        public string Ban { get; set; }
        public int? Hybernate { get; set; }
        public int? DonorId { get; set; }
        public string Referal { get; set; }
        public int? RefSum { get; set; }
        public bool? IsDailyBilling { get; set; }
        public int? CompanyIdCompany { get; set; }
        public short? DayAccrual { get; set; }
        public string Payer { get; set; }
        public bool? IsOptimal { get; set; }
        public string CityNumber { get; set; }

        public virtual ActivityStatus ActivityStatusIdActivityStatusNavigation { get; set; }
        public virtual AddStatus AddStatusIdAddStatusNavigation { get; set; }
        public virtual Agent AgentIdAgentNavigation { get; set; }
        public virtual Client ClientIdClientNavigation { get; set; }
        public virtual Company CompanyIdCompanyNavigation { get; set; }
        public virtual Tariff FrontIdTariffNavigation { get; set; }
        public virtual ProfileGnk ProfileGnkIdProfileGnkNavigation { get; set; }
        public virtual Tariff TariffIdTariffNavigation { get; set; }
        public virtual ICollection<Accrual> Accrual { get; set; }
        public virtual ICollection<AccrualDetailed> AccrualDetailed { get; set; }
        public virtual ICollection<ClientPhoneNumberConnectedToTariffOption> ClientPhoneNumberConnectedToTariffOption { get; set; }
        public virtual ICollection<Correct> Correct { get; set; }
        public virtual ICollection<Details> Details { get; set; }
        public virtual ICollection<Discount> Discount { get; set; }
        public virtual ICollection<Invoice> Invoice { get; set; }
        public virtual ICollection<Payment> Payment { get; set; }
        public virtual ICollection<PaymentDistributed> PaymentDistributed { get; set; }
        public virtual ICollection<Task> Task { get; set; }
    }
}
