﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class TaskType
    {
        public TaskType()
        {
            Task = new HashSet<Task>();
            TaskByMessage = new HashSet<TaskByMessage>();
        }

        public int IdTaskType { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Task> Task { get; set; }
        public virtual ICollection<TaskByMessage> TaskByMessage { get; set; }
    }
}
