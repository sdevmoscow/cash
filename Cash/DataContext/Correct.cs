﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Correct
    {
        public int IdCorrect { get; set; }
        public double Value { get; set; }
        public DateTime Date { get; set; }
        public string ContractNumber { get; set; }
        public string BillingGroup { get; set; }
        public int ClientPhoneNumberIdPhoneNumber { get; set; }
        public DateTime? AddDate { get; set; }

        public virtual ClientPhoneNumber ClientPhoneNumberIdPhoneNumberNavigation { get; set; }
    }
}
