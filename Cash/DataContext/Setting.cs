﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Setting
    {
        public int Id { get; set; }
        public int LastProcessedMessageId { get; set; }
        public int? New { get; set; }
        public int? SmsCnt { get; set; }
        public int? Activ { get; set; }
        public int? Blok { get; set; }
        public int? Ish { get; set; }
        public int? Credinform { get; set; }
        public DateTime? DtBalanceInform { get; set; }
        public DateTime? Dtdop { get; set; }
        public string Infomun1 { get; set; }
        public string Infonum2 { get; set; }
        public DateTime? Dtmaininv { get; set; }
        public DateTime? Dtdopinv { get; set; }
    }
}
