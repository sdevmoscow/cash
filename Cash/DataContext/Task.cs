﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Task
    {
        public int IdTask { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateFinish { get; set; }
        public double Progress { get; set; }
        public string Comment { get; set; }
        public int TaskTypeIdTaskType { get; set; }
        public int UserPerformerIdUser { get; set; }
        public int UserCustomerIdUser { get; set; }
        public int? TriggerIdTrigger { get; set; }
        public int Priority { get; set; }
        public bool IsDone { get; set; }
        public int TargetPhone { get; set; }
        public string WhatDone { get; set; }
        public DateTime? DtDone { get; set; }
        public int? UserRoleIdUserRole { get; set; }

        public virtual PriorityType PriorityNavigation { get; set; }
        public virtual ClientPhoneNumber TargetPhoneNavigation { get; set; }
        public virtual TaskType TaskTypeIdTaskTypeNavigation { get; set; }
        public virtual Trigger TriggerIdTriggerNavigation { get; set; }
        public virtual User UserCustomerIdUserNavigation { get; set; }
        public virtual User UserPerformerIdUserNavigation { get; set; }
        public virtual UserRole UserRoleIdUserRoleNavigation { get; set; }
    }
}
