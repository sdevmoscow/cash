﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class CallType
    {
        public CallType()
        {
            Details = new HashSet<Details>();
        }

        public int IdCallType { get; set; }
        public string CallType1 { get; set; }
        public int? IdService { get; set; }

        public virtual ServiceName IdServiceNavigation { get; set; }
        public virtual ICollection<Details> Details { get; set; }
    }
}
