﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Consump
    {
        public int Id { get; set; }
        public long Phonenumber { get; set; }
        public int LocalAll { get; set; }
        public int LocalOut { get; set; }
        public int LocalBee { get; set; }
        public int LocalIn { get; set; }
        public decimal LocalSum { get; set; }
        public int MgOut { get; set; }
        public int MgBee { get; set; }
        public decimal MgSum { get; set; }
        public int Mms { get; set; }
        public decimal MmsSum { get; set; }
        public int SmsAll { get; set; }
        public int SmsMg { get; set; }
        public decimal SmsSum { get; set; }
        public int MnAll { get; set; }
        public decimal MnSum { get; set; }
        public int RoamIn { get; set; }
        public int RoamOut { get; set; }
        public int RoamSms { get; set; }
        public decimal RoamSum { get; set; }
        public int InetMb { get; set; }
        public decimal InetSum { get; set; }
        public DateTime Timestamp { get; set; }
        public decimal? LocalBeeSum { get; set; }
        public decimal? MgBeeSum { get; set; }
        public decimal? SmsMgSum { get; set; }
        public int? SmsBee { get; set; }
        public decimal? SmsBeeSum { get; set; }
        public int? SmsMgBee { get; set; }
        public decimal? SmsMgBeeSum { get; set; }
        public int? MnSms { get; set; }
        public decimal? MnSmsSum { get; set; }
        public decimal? RoamOutSum { get; set; }
        public decimal? RoamSmsSum { get; set; }
        public int? CpaSms { get; set; }
        public decimal? CpaSmsSum { get; set; }
        public int? RfRoamOut { get; set; }
        public decimal? RfRoamOutSum { get; set; }
        public int? RfRoamOutBee { get; set; }
        public decimal? RfRoamOutBeeSum { get; set; }
        public int? RfRoamIn { get; set; }
        public decimal? RfRoamInSum { get; set; }
        public int? RfRoamSms { get; set; }
        public decimal? RfRoamSmsSum { get; set; }
        public int? RoamGprs { get; set; }
        public decimal? RoamGprsSum { get; set; }
        public decimal? Rc { get; set; }
        public decimal? Uc { get; set; }
        public decimal? Oc { get; set; }
    }
}
