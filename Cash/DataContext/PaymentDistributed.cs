﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class PaymentDistributed
    {
        public int IdPaymentDistributed { get; set; }
        public double? Value { get; set; }
        public int ClientPhoneNumberIdPhoneNumber { get; set; }
        public int PaymentIdPayment { get; set; }

        public virtual ClientPhoneNumber ClientPhoneNumberIdPhoneNumberNavigation { get; set; }
        public virtual Payment PaymentIdPaymentNavigation { get; set; }
    }
}
