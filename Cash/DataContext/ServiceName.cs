﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class ServiceName
    {
        public ServiceName()
        {
            CallType = new HashSet<CallType>();
        }

        public int IdService { get; set; }
        public string ServiceName1 { get; set; }

        public virtual ICollection<CallType> CallType { get; set; }
    }
}
