﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class SmsService
    {
        public int Id { get; set; }
        public string Num { get; set; }
        public string Text { get; set; }
        public int? Status { get; set; }
    }
}
