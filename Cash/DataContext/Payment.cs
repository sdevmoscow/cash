﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Payment
    {
        public Payment()
        {
            PaymentDistributed = new HashSet<PaymentDistributed>();
        }

        public int IdPayment { get; set; }
        public double Value { get; set; }
        public DateTime? Date { get; set; }
        public string No { get; set; }
        public string Status { get; set; }
        public DateTime? DateImport { get; set; }
        public int ClientPhoneNumberIdPhoneNumber { get; set; }
        public int? PaymentStatusIdPaymentStatus { get; set; }
        public int? PaymentTypeIdPaymentType { get; set; }
        public string Info { get; set; }
        public bool Ofd { get; set; }
        public string Receipt { get; set; }

        public virtual ClientPhoneNumber ClientPhoneNumberIdPhoneNumberNavigation { get; set; }
        public virtual PaymentStatus PaymentStatusIdPaymentStatusNavigation { get; set; }
        public virtual PaymentType PaymentTypeIdPaymentTypeNavigation { get; set; }
        public virtual ICollection<PaymentDistributed> PaymentDistributed { get; set; }
    }
}
