﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Tariff
    {
        public Tariff()
        {
            ClientPhoneNumberFrontIdTariffNavigation = new HashSet<ClientPhoneNumber>();
            ClientPhoneNumberTariffIdTariffNavigation = new HashSet<ClientPhoneNumber>();
            TariffTariffOption = new HashSet<TariffTariffOption>();
        }

        public int IdTariff { get; set; }
        public string Name { get; set; }
        public double CostClient { get; set; }
        public double Cost { get; set; }
        public string Code { get; set; }
        public int? LocalOut { get; set; }
        public int? LocalBee { get; set; }
        public int? LocalSms { get; set; }
        public int? MgOut { get; set; }
        public int? MgBee { get; set; }
        public int? MgSms { get; set; }
        public int? MnIn { get; set; }
        public int? MnOut { get; set; }
        public int? MnSms { get; set; }
        public int? MnGprs { get; set; }
        public int? LocalMnOut { get; set; }
        public int? LocalGprs { get; set; }
        public int? RoamIn { get; set; }
        public int? RoamOut { get; set; }
        public int? RoamSms { get; set; }
        public int? RoamGprs { get; set; }
        public int? RoamBee { get; set; }
        public string Description { get; set; }
        public int? LocalSmsBee { get; set; }
        public int? MgSmsBee { get; set; }
        public int? CallPacket { get; set; }
        public int? SmsPacket { get; set; }
        public int? DataPacket { get; set; }
        public string EntityDesc { get; set; }
        public string RcRatePeriod { get; set; }
        public int? RcRate { get; set; }
        public int? ChargeAmount { get; set; }
        public string Category { get; set; }
        public double? LocalOutCost { get; set; }
        public double? LocalBeeCost { get; set; }
        public double? MgOutCost { get; set; }
        public double? MgBeeCost { get; set; }
        public double? RoamInCost { get; set; }
        public double? RoamOutCost { get; set; }
        public double? RoamBeeCost { get; set; }
        public int? Type { get; set; }
        public int? AgentIdAgent { get; set; }

        public virtual Agent AgentIdAgentNavigation { get; set; }
        public virtual ICollection<ClientPhoneNumber> ClientPhoneNumberFrontIdTariffNavigation { get; set; }
        public virtual ICollection<ClientPhoneNumber> ClientPhoneNumberTariffIdTariffNavigation { get; set; }
        public virtual ICollection<TariffTariffOption> TariffTariffOption { get; set; }
    }
}
