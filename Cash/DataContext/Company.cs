﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Company
    {
        public Company()
        {
            ClientPhoneNumber = new HashSet<ClientPhoneNumber>();
        }

        public int IdCompany { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Inn { get; set; }
        public string Kpp { get; set; }
        public string Ogrn { get; set; }
        public string Adr { get; set; }
        public string Rekv { get; set; }
        public string Comment { get; set; }
        public string Director { get; set; }
        public int? BillingMode { get; set; }
        public int? BillingDay { get; set; }

        public virtual ICollection<ClientPhoneNumber> ClientPhoneNumber { get; set; }
    }
}
