﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Providers
    {
        public int IdProvider { get; set; }
        public string ProvName { get; set; }
    }
}
