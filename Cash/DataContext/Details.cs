﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Details
    {
        public int Id { get; set; }
        public DateTime CallDate { get; set; }
        public string CallNumber { get; set; }
        public string CallToNumber { get; set; }
        public decimal? DataVolume { get; set; }
        public decimal? CallAmt { get; set; }
        public string CallDuration { get; set; }
        public int? IdCpn { get; set; }
        public int? IdCallType { get; set; }

        public virtual CallType IdCallTypeNavigation { get; set; }
        public virtual ClientPhoneNumber IdCpnNavigation { get; set; }
    }
}
