﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class TaskByMessage
    {
        public int IdTaskByMessage { get; set; }
        public string Message { get; set; }
        public int? SecondsToExecuteTask { get; set; }
        public int TaskTypeIdTaskType { get; set; }
        public int? Priority { get; set; }
        public int? UserPerformerIdUser { get; set; }

        public virtual PriorityType PriorityNavigation { get; set; }
        public virtual TaskType TaskTypeIdTaskTypeNavigation { get; set; }
        public virtual User UserPerformerIdUserNavigation { get; set; }
    }
}
