﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class AccrualDetailed
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string Tariff { get; set; }
        public double InternationalCalls { get; set; }
        public double IntercityCalls { get; set; }
        public double LocalCalls { get; set; }
        public double? LocalMessage { get; set; }
        public double? Gprs { get; set; }
        public double? InternationalRoamingTalktimePartner { get; set; }
        public double? InternationalRoamingSmsPartner { get; set; }
        public double? InternationalRoamingGprsPartner { get; set; }
        public double? InternationalRoaming { get; set; }
        public double? NationalRoamingTalktimePartner { get; set; }
        public double? NationalRoamingSmsPartner { get; set; }
        public double? NationalRoamingGprsPartner { get; set; }
        public double? NationalRoaming { get; set; }
        public double LicenseFee { get; set; }
        public double LicenseFeeAdditional { get; set; }
        public double Discount { get; set; }
        public double SingleAccruals { get; set; }
        public int IdAccrualDetailed { get; set; }
        public double MainSum { get; set; }
        public double AddSum { get; set; }
        public int ClientPhoneNumberIdPhoneNumber { get; set; }
        public double? LatePayment { get; set; }
        public string Ban { get; set; }
        public string Ben { get; set; }

        public virtual ClientPhoneNumber ClientPhoneNumberIdPhoneNumberNavigation { get; set; }
    }
}
