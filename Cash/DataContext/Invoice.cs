﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Invoice
    {
        public int IdInvoice { get; set; }
        public double Value { get; set; }
        public DateTime? Date { get; set; }
        public bool IsPaid { get; set; }
        public bool IsAdd { get; set; }
        public bool IsPreInvoice { get; set; }
        public bool IsAuto { get; set; }
        public DateTime? DateImport { get; set; }
        public string Comment { get; set; }
        public int ClientPhoneNumberIdPhoneNumber { get; set; }
        public int? Base { get; set; }
        public int? Options { get; set; }
        public int? Rebill { get; set; }

        public virtual ClientPhoneNumber ClientPhoneNumberIdPhoneNumberNavigation { get; set; }
    }
}
