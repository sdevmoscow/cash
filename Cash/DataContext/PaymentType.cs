﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class PaymentType
    {
        public PaymentType()
        {
            Client = new HashSet<Client>();
            Payment = new HashSet<Payment>();
        }

        public int IdPaymentType { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Client> Client { get; set; }
        public virtual ICollection<Payment> Payment { get; set; }
    }
}
