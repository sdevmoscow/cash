﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class AddStatus
    {
        public AddStatus()
        {
            ClientPhoneNumber = new HashSet<ClientPhoneNumber>();
        }

        public int IdAddStatus { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ClientPhoneNumber> ClientPhoneNumber { get; set; }
    }
}
