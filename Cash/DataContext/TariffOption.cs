﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class TariffOption
    {
        public TariffOption()
        {
            ClientPhoneNumberConnectedToTariffOption = new HashSet<ClientPhoneNumberConnectedToTariffOption>();
            TariffTariffOption = new HashSet<TariffTariffOption>();
        }

        public int IdTariffOption { get; set; }
        public string Name { get; set; }
        public double Cost { get; set; }
        public double CostClient { get; set; }
        public string Code { get; set; }
        public int? IdGroup { get; set; }
        public string GroupName { get; set; }
        public int? Weight { get; set; }
        public string EntityDesc { get; set; }
        public decimal? RcRate { get; set; }
        public string RcRatePeriod { get; set; }
        public string RcRatePeriodText { get; set; }
        public string Category { get; set; }
        public decimal? SdbSize { get; set; }
        public string BaseFeatures { get; set; }
        public decimal? ChargeAmount { get; set; }

        public virtual ICollection<ClientPhoneNumberConnectedToTariffOption> ClientPhoneNumberConnectedToTariffOption { get; set; }
        public virtual ICollection<TariffTariffOption> TariffTariffOption { get; set; }
    }
}
