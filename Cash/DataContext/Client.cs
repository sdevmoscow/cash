﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Client
    {
        public Client()
        {
            ClientPhoneNumber = new HashSet<ClientPhoneNumber>();
        }

        public int IdClient { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public string SurName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
        public int? PaymentTypeIdPaymentType { get; set; }
        public DateTime? BirthDate { get; set; }
        public int? DocumentType { get; set; }
        public string DocumentNum { get; set; }
        public string DocumentId { get; set; }
        public DateTime? DocIssuDate { get; set; }
        public string DocIssuOrigin { get; set; }
        public string Country { get; set; }
        public string PlaceType { get; set; }
        public string PlaceNameCity { get; set; }
        public string StreetType { get; set; }
        public string StreetName { get; set; }
        public string HouseNo { get; set; }
        public string BuildingType { get; set; }
        public string BuildingNo { get; set; }
        public string ApartType { get; set; }
        public string ApartNo { get; set; }
        public bool? Dkpready { get; set; }

        public virtual PaymentType PaymentTypeIdPaymentTypeNavigation { get; set; }
        public virtual ICollection<ClientPhoneNumber> ClientPhoneNumber { get; set; }
    }
}
