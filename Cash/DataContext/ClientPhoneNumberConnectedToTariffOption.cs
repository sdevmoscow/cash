﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class ClientPhoneNumberConnectedToTariffOption
    {
        public int IdClientPhoneNumberConnectedToTariffOption { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int TariffOptionIdTariffOption { get; set; }
        public int ClientPhoneNumberIdPhoneNumber { get; set; }

        public virtual ClientPhoneNumber ClientPhoneNumberIdPhoneNumberNavigation { get; set; }
        public virtual TariffOption TariffOptionIdTariffOptionNavigation { get; set; }
    }
}
