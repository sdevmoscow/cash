﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class User
    {
        public User()
        {
            TaskByMessage = new HashSet<TaskByMessage>();
            TaskUserCustomerIdUserNavigation = new HashSet<Task>();
            TaskUserPerformerIdUserNavigation = new HashSet<Task>();
        }

        public int IdUser { get; set; }
        public string Name { get; set; }
        public string PassHash { get; set; }
        public int UserRoleIdUserRole { get; set; }

        public virtual UserRole UserRoleIdUserRoleNavigation { get; set; }
        public virtual ICollection<TaskByMessage> TaskByMessage { get; set; }
        public virtual ICollection<Task> TaskUserCustomerIdUserNavigation { get; set; }
        public virtual ICollection<Task> TaskUserPerformerIdUserNavigation { get; set; }
    }
}
