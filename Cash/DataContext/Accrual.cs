﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Accrual
    {
        public int IdAccrual { get; set; }
        public double Value { get; set; }
        public DateTime Date { get; set; }
        public int ClientPhoneNumberIdPhoneNumber { get; set; }

        public virtual ClientPhoneNumber ClientPhoneNumberIdPhoneNumberNavigation { get; set; }
    }
}
