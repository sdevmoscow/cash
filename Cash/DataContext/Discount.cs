﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Discount
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public double Value { get; set; }
        public int ClientPhoneNumberIdPhoneNumber { get; set; }

        public virtual ClientPhoneNumber ClientPhoneNumberIdPhoneNumberNavigation { get; set; }
    }
}
