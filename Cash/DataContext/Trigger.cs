﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Trigger
    {
        public Trigger()
        {
            Task = new HashSet<Task>();
        }

        public int IdTrigger { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public string Code { get; set; }
        public int TriggerTypeIdTriggerType { get; set; }

        public virtual TriggerType TriggerTypeIdTriggerTypeNavigation { get; set; }
        public virtual ICollection<Task> Task { get; set; }
    }
}
