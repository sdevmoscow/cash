﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Sms
    {
        public int Id { get; set; }
        public int? SmsDirect { get; set; }
        public string SmsPhone { get; set; }
        public string SmsText { get; set; }
        public DateTime SmsTime { get; set; }
    }
}
