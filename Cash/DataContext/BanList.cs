﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class BanList
    {
        public int BanId { get; set; }
        public string Ban { get; set; }
        public string Login { get; set; }
        public string Pass { get; set; }
        public string Description { get; set; }
        public int? Agent { get; set; }
    }
}
