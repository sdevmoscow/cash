﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class SmsPattern
    {
        public int IdSmsPattern { get; set; }
        public string Text { get; set; }
        public string ButtonText { get; set; }
    }
}
