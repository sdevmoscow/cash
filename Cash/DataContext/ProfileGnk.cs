﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class ProfileGnk
    {
        public ProfileGnk()
        {
            ClientPhoneNumber = new HashSet<ClientPhoneNumber>();
        }

        public int IdProfileGnk { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ClientPhoneNumber> ClientPhoneNumber { get; set; }
    }
}
