﻿using System;
using System.Collections.Generic;

namespace Cash.DataContext
{
    public partial class Requests
    {
        public int ReqId { get; set; }
        public long RequestId { get; set; }
        public string RequestStatus { get; set; }
        public string RequestType { get; set; }
        public string RequestSubType { get; set; }
        public string RequestComments { get; set; }
        public DateTime RequestDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Ban { get; set; }
    }
}
