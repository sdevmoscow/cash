﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace Cash.DataContext
{
    public partial class MobileBilling4Context : DbContext
    {
        public MobileBilling4Context()
        {
        }

        public MobileBilling4Context(DbContextOptions<MobileBilling4Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Accrual> Accrual { get; set; }
        public virtual DbSet<AccrualDetailed> AccrualDetailed { get; set; }
        public virtual DbSet<ActivityStatus> ActivityStatus { get; set; }
        public virtual DbSet<AddStatus> AddStatus { get; set; }
        public virtual DbSet<Agent> Agent { get; set; }
        public virtual DbSet<BanList> BanList { get; set; }
        public virtual DbSet<CallType> CallType { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<ClientPhoneNumber> ClientPhoneNumber { get; set; }
        public virtual DbSet<ClientPhoneNumberConnectedToTariffOption> ClientPhoneNumberConnectedToTariffOption { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<Consump> Consump { get; set; }
        public virtual DbSet<Correct> Correct { get; set; }
        public virtual DbSet<Details> Details { get; set; }
        public virtual DbSet<Discount> Discount { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<PaymentDistributed> PaymentDistributed { get; set; }
        public virtual DbSet<PaymentStatus> PaymentStatus { get; set; }
        public virtual DbSet<PaymentType> PaymentType { get; set; }
        public virtual DbSet<PriorityType> PriorityType { get; set; }
        public virtual DbSet<ProfileGnk> ProfileGnk { get; set; }
        public virtual DbSet<Providers> Providers { get; set; }
        public virtual DbSet<Requests> Requests { get; set; }
        public virtual DbSet<Serv> Serv { get; set; }
        public virtual DbSet<ServiceName> ServiceName { get; set; }
        public virtual DbSet<Setting> Setting { get; set; }
        public virtual DbSet<Sms> Sms { get; set; }
        public virtual DbSet<SmsPattern> SmsPattern { get; set; }
        public virtual DbSet<SmsService> SmsService { get; set; }
        public virtual DbSet<Tariff> Tariff { get; set; }
        public virtual DbSet<TariffOption> TariffOption { get; set; }
        public virtual DbSet<TariffTariffOption> TariffTariffOption { get; set; }
        public virtual DbSet<Task> Task { get; set; }
        public virtual DbSet<TaskByMessage> TaskByMessage { get; set; }
        public virtual DbSet<TaskType> TaskType { get; set; }
        public virtual DbSet<Trigger> Trigger { get; set; }
        public virtual DbSet<TriggerType> TriggerType { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            /*if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=95.211.15.131\\SQLEXPRESS;Database=MobileBilling4;user id=bill;password=bill");
            }*/
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json")
                            .Build();
                var connectionString = configuration["BillingConnectionString"];
                Helpers.Helper.Log("I", "Connection string: " + connectionString);
                optionsBuilder.UseSqlServer(connectionString);
                
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Accrual>(entity =>
            {
                entity.HasKey(e => e.IdAccrual);

                entity.HasIndex(e => e.ClientPhoneNumberIdPhoneNumber)
                    .HasName("IX_FK_ClientPhoneNumberAccrual");

                entity.Property(e => e.IdAccrual).HasColumnName("Id_Accrual");

                entity.Property(e => e.ClientPhoneNumberIdPhoneNumber).HasColumnName("ClientPhoneNumber_Id_PhoneNumber");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.HasOne(d => d.ClientPhoneNumberIdPhoneNumberNavigation)
                    .WithMany(p => p.Accrual)
                    .HasForeignKey(d => d.ClientPhoneNumberIdPhoneNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientPhoneNumberAccrual");
            });

            modelBuilder.Entity<AccrualDetailed>(entity =>
            {
                entity.HasKey(e => e.IdAccrualDetailed);

                entity.HasIndex(e => e.ClientPhoneNumberIdPhoneNumber)
                    .HasName("IX_FK_ClientsAccounts");

                entity.Property(e => e.IdAccrualDetailed).HasColumnName("Id_AccrualDetailed");

                entity.Property(e => e.Ban)
                    .HasColumnName("ban")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Ben)
                    .HasColumnName("ben")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.ClientPhoneNumberIdPhoneNumber).HasColumnName("ClientPhoneNumber_Id_PhoneNumber");

                entity.Property(e => e.DateFrom).HasColumnType("datetime");

                entity.Property(e => e.DateTo).HasColumnType("datetime");

                entity.Property(e => e.Gprs).HasColumnName("GPRS");

                entity.Property(e => e.LatePayment).HasColumnName("latePayment");

                entity.Property(e => e.Tariff).IsRequired();

                entity.HasOne(d => d.ClientPhoneNumberIdPhoneNumberNavigation)
                    .WithMany(p => p.AccrualDetailed)
                    .HasForeignKey(d => d.ClientPhoneNumberIdPhoneNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientsAccounts");
            });

            modelBuilder.Entity<ActivityStatus>(entity =>
            {
                entity.HasKey(e => e.IdActivityStatus);

                entity.Property(e => e.IdActivityStatus).HasColumnName("Id_ActivityStatus");

                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<AddStatus>(entity =>
            {
                entity.HasKey(e => e.IdAddStatus);

                entity.Property(e => e.IdAddStatus).HasColumnName("Id_AddStatus");

                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<Agent>(entity =>
            {
                entity.HasKey(e => e.IdAgent);

                entity.Property(e => e.IdAgent).HasColumnName("Id_Agent");

                entity.Property(e => e.Comment).IsRequired();

                entity.Property(e => e.Email).IsRequired();

                entity.Property(e => e.MiddleName).IsRequired();

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.PhoneNumber).IsRequired();

                entity.Property(e => e.Rate).IsRequired();

                entity.Property(e => e.Surname).IsRequired();
            });

            modelBuilder.Entity<BanList>(entity =>
            {
                entity.HasKey(e => e.BanId);

                entity.Property(e => e.BanId).HasColumnName("ban_id");

                entity.Property(e => e.Agent).HasColumnName("agent");

                entity.Property(e => e.Ban)
                    .HasColumnName("ban")
                    .HasMaxLength(9)
                    .IsFixedLength();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(100);

                entity.Property(e => e.Login)
                    .HasColumnName("login")
                    .HasMaxLength(20);

                entity.Property(e => e.Pass)
                    .HasColumnName("pass")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<CallType>(entity =>
            {
                entity.HasKey(e => e.IdCallType);

                entity.Property(e => e.CallType1)
                    .IsRequired()
                    .HasColumnName("CallType")
                    .HasMaxLength(100);

                entity.HasOne(d => d.IdServiceNavigation)
                    .WithMany(p => p.CallType)
                    .HasForeignKey(d => d.IdService)
                    .HasConstraintName("FK_CallType_ServiceName");
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.HasKey(e => e.IdClient);

                entity.HasIndex(e => e.PaymentTypeIdPaymentType)
                    .HasName("IX_FK_PaymentTypeClient");

                entity.Property(e => e.IdClient).HasColumnName("Id_Client");

                entity.Property(e => e.ApartNo)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.ApartType)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.BirthDate).HasColumnType("datetime");

                entity.Property(e => e.BuildingNo)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.BuildingType)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.Dkpready).HasColumnName("DKPReady");

                entity.Property(e => e.DocIssuDate).HasColumnType("datetime");

                entity.Property(e => e.DocumentId)
                    .HasColumnName("DocumentID")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.DocumentNum)
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.HouseNo)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.PaymentTypeIdPaymentType).HasColumnName("PaymentType_Id_PaymentType");

                entity.Property(e => e.PlaceNameCity)
                    .HasMaxLength(50)
                    .IsFixedLength();

                entity.Property(e => e.PlaceType)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.StreetType)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.SurName).IsRequired();

                entity.HasOne(d => d.PaymentTypeIdPaymentTypeNavigation)
                    .WithMany(p => p.Client)
                    .HasForeignKey(d => d.PaymentTypeIdPaymentType)
                    .HasConstraintName("FK_PaymentTypeClient");
            });

            modelBuilder.Entity<ClientPhoneNumber>(entity =>
            {
                entity.HasKey(e => e.IdPhoneNumber);

                entity.HasIndex(e => e.ActivityStatusIdActivityStatus)
                    .HasName("IX_FK_ClientPhoneNumberActivityStatus");

                entity.HasIndex(e => e.AddStatusIdAddStatus)
                    .HasName("IX_FK_ClientPhoneNumberAddStatus");

                entity.HasIndex(e => e.AgentIdAgent)
                    .HasName("IX_FK_AgentClientPhoneNumber");

                entity.HasIndex(e => e.ClientIdClient)
                    .HasName("IX_FK_ClientClientPhoneNumber");

                entity.HasIndex(e => e.DiscountIdDiscount)
                    .HasName("IX_FK_DiscountClientPhoneNumber");

                entity.HasIndex(e => e.ProfileGnkIdProfileGnk)
                    .HasName("IX_FK_ProfileGNKClientPhoneNumber");

                entity.HasIndex(e => e.TariffIdTariff)
                    .HasName("IX_FK_TariffClientPhoneNumber");

                entity.Property(e => e.IdPhoneNumber).HasColumnName("Id_PhoneNumber");

                entity.Property(e => e.ActivityStatusIdActivityStatus).HasColumnName("ActivityStatus_Id_ActivityStatus");

                entity.Property(e => e.AddStatusIdAddStatus).HasColumnName("AddStatus_Id_AddStatus");

                entity.Property(e => e.AgentIdAgent).HasColumnName("Agent_Id_Agent");

                entity.Property(e => e.Ban)
                    .HasColumnName("ban")
                    .HasMaxLength(9)
                    .IsFixedLength();

                entity.Property(e => e.CityNumber).HasMaxLength(10);

                entity.Property(e => e.ClientIdClient).HasColumnName("Client_Id_Client");

                entity.Property(e => e.CompanyIdCompany).HasColumnName("Company_Id_Company");

                entity.Property(e => e.DataLimit).HasColumnName("Data_Limit");

                entity.Property(e => e.DateActivateFirst).HasColumnType("datetime");

                entity.Property(e => e.DateBlock).HasColumnType("datetime");

                entity.Property(e => e.DateChangePp)
                    .HasColumnName("dateChangePP")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateRebil).HasColumnType("datetime");

                entity.Property(e => e.DiscountIdDiscount).HasColumnName("Discount_Id_Discount");

                entity.Property(e => e.DonorId).HasColumnName("Donor_Id");

                entity.Property(e => e.Dtblk)
                    .HasColumnName("dtblk")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dtinfabon)
                    .HasColumnName("dtinfabon")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dtinfdop)
                    .HasColumnName("dtinfdop")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dtstat).HasColumnName("dtstat");

                entity.Property(e => e.Dtup)
                    .HasColumnName("dtup")
                    .HasColumnType("datetime");

                entity.Property(e => e.FrontIdTariff).HasColumnName("Front_Id_Tariff");

                entity.Property(e => e.Hlr).HasColumnName("hlr");

                entity.Property(e => e.Iccid)
                    .HasColumnName("iccid")
                    .HasMaxLength(50);

                entity.Property(e => e.Info).HasMaxLength(500);

                entity.Property(e => e.IsDonor).HasColumnName("isDonor");

                entity.Property(e => e.Oc).HasColumnName("OC");

                entity.Property(e => e.Pass)
                    .HasColumnName("pass")
                    .HasMaxLength(50);

                entity.Property(e => e.Payer)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.ProfileGnkIdProfileGnk).HasColumnName("ProfileGNK_Id_ProfileGNK");

                entity.Property(e => e.Rc).HasColumnName("RC");

                entity.Property(e => e.RefSum).HasColumnName("Ref_Sum");

                entity.Property(e => e.Referal)
                    .HasMaxLength(50)
                    .IsFixedLength();

                entity.Property(e => e.TariffIdTariff).HasColumnName("Tariff_Id_Tariff");

                entity.Property(e => e.Uc).HasColumnName("UC");

                entity.HasOne(d => d.ActivityStatusIdActivityStatusNavigation)
                    .WithMany(p => p.ClientPhoneNumber)
                    .HasForeignKey(d => d.ActivityStatusIdActivityStatus)
                    .HasConstraintName("FK_ClientPhoneNumberActivityStatus");

                entity.HasOne(d => d.AddStatusIdAddStatusNavigation)
                    .WithMany(p => p.ClientPhoneNumber)
                    .HasForeignKey(d => d.AddStatusIdAddStatus)
                    .HasConstraintName("FK_ClientPhoneNumberAddStatus");

                entity.HasOne(d => d.AgentIdAgentNavigation)
                    .WithMany(p => p.ClientPhoneNumber)
                    .HasForeignKey(d => d.AgentIdAgent)
                    .HasConstraintName("FK_AgentClientPhoneNumber");

                entity.HasOne(d => d.ClientIdClientNavigation)
                    .WithMany(p => p.ClientPhoneNumber)
                    .HasForeignKey(d => d.ClientIdClient)
                    .HasConstraintName("FK_ClientClientPhoneNumber");

                entity.HasOne(d => d.CompanyIdCompanyNavigation)
                    .WithMany(p => p.ClientPhoneNumber)
                    .HasForeignKey(d => d.CompanyIdCompany)
                    .HasConstraintName("FK_CompanyClientPhoneNumber");

                entity.HasOne(d => d.FrontIdTariffNavigation)
                    .WithMany(p => p.ClientPhoneNumberFrontIdTariffNavigation)
                    .HasForeignKey(d => d.FrontIdTariff)
                    .HasConstraintName("FK_FrontTariff");

                entity.HasOne(d => d.ProfileGnkIdProfileGnkNavigation)
                    .WithMany(p => p.ClientPhoneNumber)
                    .HasForeignKey(d => d.ProfileGnkIdProfileGnk)
                    .HasConstraintName("FK_ProfileGNKClientPhoneNumber");

                entity.HasOne(d => d.TariffIdTariffNavigation)
                    .WithMany(p => p.ClientPhoneNumberTariffIdTariffNavigation)
                    .HasForeignKey(d => d.TariffIdTariff)
                    .HasConstraintName("FK_TariffClientPhoneNumber");
            });

            modelBuilder.Entity<ClientPhoneNumberConnectedToTariffOption>(entity =>
            {
                entity.HasKey(e => e.IdClientPhoneNumberConnectedToTariffOption)
                    .HasName("PK_ClientPhoneNumberConnectedToTariffOptionSet");

                entity.HasIndex(e => e.ClientPhoneNumberIdPhoneNumber)
                    .HasName("IX_FK_ClientPhoneNumberClientPhoneNumberConnectedToTariffOption");

                entity.HasIndex(e => e.TariffOptionIdTariffOption)
                    .HasName("IX_FK_TariffOptionClientPhoneNumberConnectedToTariffOption");

                entity.Property(e => e.IdClientPhoneNumberConnectedToTariffOption).HasColumnName("Id_ClientPhoneNumberConnectedToTariffOption");

                entity.Property(e => e.ClientPhoneNumberIdPhoneNumber).HasColumnName("ClientPhoneNumber_Id_PhoneNumber");

                entity.Property(e => e.DateFrom).HasColumnType("datetime");

                entity.Property(e => e.DateTo).HasColumnType("datetime");

                entity.Property(e => e.TariffOptionIdTariffOption).HasColumnName("TariffOption_Id_TariffOption");

                entity.HasOne(d => d.ClientPhoneNumberIdPhoneNumberNavigation)
                    .WithMany(p => p.ClientPhoneNumberConnectedToTariffOption)
                    .HasForeignKey(d => d.ClientPhoneNumberIdPhoneNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientPhoneNumberClientPhoneNumberConnectedToTariffOption");

                entity.HasOne(d => d.TariffOptionIdTariffOptionNavigation)
                    .WithMany(p => p.ClientPhoneNumberConnectedToTariffOption)
                    .HasForeignKey(d => d.TariffOptionIdTariffOption)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TariffOptionClientPhoneNumberConnectedToTariffOption");
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.HasKey(e => e.IdCompany);

                entity.Property(e => e.IdCompany).HasColumnName("Id_Company");

                entity.Property(e => e.Director)
                    .HasMaxLength(100)
                    .IsFixedLength();

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsFixedLength();

                entity.Property(e => e.Inn)
                    .HasMaxLength(12)
                    .IsFixedLength();

                entity.Property(e => e.Kpp)
                    .HasMaxLength(9)
                    .IsFixedLength();

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.Ogrn)
                    .HasMaxLength(15)
                    .IsFixedLength();

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(10)
                    .IsFixedLength();
            });

            modelBuilder.Entity<Consump>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CpaSms).HasColumnName("cpa_sms");

                entity.Property(e => e.CpaSmsSum)
                    .HasColumnName("cpa_sms_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.InetMb).HasColumnName("inet_mb");

                entity.Property(e => e.InetSum)
                    .HasColumnName("inet_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.LocalAll).HasColumnName("local_all");

                entity.Property(e => e.LocalBee).HasColumnName("local_bee");

                entity.Property(e => e.LocalBeeSum)
                    .HasColumnName("local_bee_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.LocalIn).HasColumnName("local_in");

                entity.Property(e => e.LocalOut).HasColumnName("local_out");

                entity.Property(e => e.LocalSum)
                    .HasColumnName("local_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.MgBee).HasColumnName("mg_bee");

                entity.Property(e => e.MgBeeSum)
                    .HasColumnName("mg_bee_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.MgOut).HasColumnName("mg_out");

                entity.Property(e => e.MgSum)
                    .HasColumnName("mg_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.Mms).HasColumnName("mms");

                entity.Property(e => e.MmsSum)
                    .HasColumnName("mms_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.MnAll).HasColumnName("mn_all");

                entity.Property(e => e.MnSms).HasColumnName("mn_sms");

                entity.Property(e => e.MnSmsSum)
                    .HasColumnName("mn_sms_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.MnSum)
                    .HasColumnName("mn_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.Oc)
                    .HasColumnName("OC")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.Phonenumber).HasColumnName("phonenumber");

                entity.Property(e => e.Rc)
                    .HasColumnName("RC")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.RfRoamIn).HasColumnName("rf_roam_in");

                entity.Property(e => e.RfRoamInSum)
                    .HasColumnName("rf_roam_in_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.RfRoamOut).HasColumnName("rf_roam_out");

                entity.Property(e => e.RfRoamOutBee).HasColumnName("rf_roam_out_bee");

                entity.Property(e => e.RfRoamOutBeeSum)
                    .HasColumnName("rf_roam_out_bee_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.RfRoamOutSum)
                    .HasColumnName("rf_roam_out_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.RfRoamSms).HasColumnName("rf_roam_sms");

                entity.Property(e => e.RfRoamSmsSum)
                    .HasColumnName("rf_roam_sms_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.RoamGprs).HasColumnName("roam_gprs");

                entity.Property(e => e.RoamGprsSum)
                    .HasColumnName("roam_gprs_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.RoamIn).HasColumnName("roam_in");

                entity.Property(e => e.RoamOut).HasColumnName("roam_out");

                entity.Property(e => e.RoamOutSum)
                    .HasColumnName("roam_out_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.RoamSms).HasColumnName("roam_sms");

                entity.Property(e => e.RoamSmsSum)
                    .HasColumnName("roam_sms_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.RoamSum)
                    .HasColumnName("roam_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.SmsAll).HasColumnName("sms_all");

                entity.Property(e => e.SmsBee).HasColumnName("sms_bee");

                entity.Property(e => e.SmsBeeSum)
                    .HasColumnName("sms_bee_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.SmsMg).HasColumnName("sms_mg");

                entity.Property(e => e.SmsMgBee).HasColumnName("sms_mg_bee");

                entity.Property(e => e.SmsMgBeeSum)
                    .HasColumnName("sms_mg_bee_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.SmsMgSum)
                    .HasColumnName("sms_mg_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.SmsSum)
                    .HasColumnName("sms_sum")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.Timestamp).HasColumnName("timestamp");

                entity.Property(e => e.Uc)
                    .HasColumnName("UC")
                    .HasColumnType("decimal(7, 2)");
            });

            modelBuilder.Entity<Correct>(entity =>
            {
                entity.HasKey(e => e.IdCorrect);

                entity.HasIndex(e => e.ClientPhoneNumberIdPhoneNumber)
                    .HasName("IX_FK_ClientPhoneNumberCorrect");

                entity.Property(e => e.IdCorrect).HasColumnName("Id_Correct");

                entity.Property(e => e.AddDate).HasColumnType("datetime");

                entity.Property(e => e.BillingGroup).IsRequired();

                entity.Property(e => e.ClientPhoneNumberIdPhoneNumber).HasColumnName("ClientPhoneNumber_Id_PhoneNumber");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.HasOne(d => d.ClientPhoneNumberIdPhoneNumberNavigation)
                    .WithMany(p => p.Correct)
                    .HasForeignKey(d => d.ClientPhoneNumberIdPhoneNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientPhoneNumberCorrect");
            });

            modelBuilder.Entity<Details>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CallAmt)
                    .HasColumnName("callAmt")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CallDate)
                    .HasColumnName("callDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.CallDuration).HasColumnName("callDuration");

                entity.Property(e => e.CallNumber).HasColumnName("callNumber");

                entity.Property(e => e.CallToNumber).HasColumnName("callToNumber");

                entity.Property(e => e.DataVolume)
                    .HasColumnName("dataVolume")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.IdCallType).HasColumnName("id_callType");

                entity.Property(e => e.IdCpn).HasColumnName("id_cpn");

                entity.HasOne(d => d.IdCallTypeNavigation)
                    .WithMany(p => p.Details)
                    .HasForeignKey(d => d.IdCallType)
                    .HasConstraintName("FK_Details_CallType");

                entity.HasOne(d => d.IdCpnNavigation)
                    .WithMany(p => p.Details)
                    .HasForeignKey(d => d.IdCpn)
                    .HasConstraintName("FK_Details_ClientPhoneNumber");
            });

            modelBuilder.Entity<Discount>(entity =>
            {
                entity.HasIndex(e => e.ClientPhoneNumberIdPhoneNumber)
                    .HasName("IX_FK_ClientPhoneNumberDiscount");

                entity.Property(e => e.ClientPhoneNumberIdPhoneNumber).HasColumnName("ClientPhoneNumber_Id_PhoneNumber");

                entity.Property(e => e.DateFrom).HasColumnType("datetime");

                entity.Property(e => e.DateTo).HasColumnType("datetime");

                entity.Property(e => e.Name).IsRequired();

                entity.HasOne(d => d.ClientPhoneNumberIdPhoneNumberNavigation)
                    .WithMany(p => p.Discount)
                    .HasForeignKey(d => d.ClientPhoneNumberIdPhoneNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientPhoneNumberDiscount");
            });

            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.HasKey(e => e.IdInvoice);

                entity.HasIndex(e => e.ClientPhoneNumberIdPhoneNumber)
                    .HasName("IX_FK_ClientPhoneNumberInvoice");

                entity.Property(e => e.IdInvoice).HasColumnName("Id_Invoice");

                entity.Property(e => e.ClientPhoneNumberIdPhoneNumber).HasColumnName("ClientPhoneNumber_Id_PhoneNumber");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.DateImport).HasColumnType("datetime");

                entity.HasOne(d => d.ClientPhoneNumberIdPhoneNumberNavigation)
                    .WithMany(p => p.Invoice)
                    .HasForeignKey(d => d.ClientPhoneNumberIdPhoneNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientPhoneNumberInvoice");
            });

            modelBuilder.Entity<Payment>(entity =>
            {
                entity.HasKey(e => e.IdPayment);

                entity.HasIndex(e => e.ClientPhoneNumberIdPhoneNumber)
                    .HasName("IX_FK_ClientPhoneNumberPayment");

                entity.HasIndex(e => e.PaymentStatusIdPaymentStatus)
                    .HasName("IX_FK_PaymentStatusPayment");

                entity.HasIndex(e => e.PaymentTypeIdPaymentType)
                    .HasName("IX_FK_PaymentTypePayment");

                entity.Property(e => e.IdPayment).HasColumnName("Id_Payment");

                entity.Property(e => e.ClientPhoneNumberIdPhoneNumber).HasColumnName("ClientPhoneNumber_Id_PhoneNumber");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.DateImport).HasColumnType("datetime");

                entity.Property(e => e.PaymentStatusIdPaymentStatus).HasColumnName("PaymentStatus_Id_PaymentStatus");

                entity.Property(e => e.PaymentTypeIdPaymentType).HasColumnName("PaymentType_Id_PaymentType");

                entity.Property(e => e.Status).IsRequired();

                entity.HasOne(d => d.ClientPhoneNumberIdPhoneNumberNavigation)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.ClientPhoneNumberIdPhoneNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientPhoneNumberPayment");

                entity.HasOne(d => d.PaymentStatusIdPaymentStatusNavigation)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.PaymentStatusIdPaymentStatus)
                    .HasConstraintName("FK_PaymentStatusPayment");

                entity.HasOne(d => d.PaymentTypeIdPaymentTypeNavigation)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.PaymentTypeIdPaymentType)
                    .HasConstraintName("FK_PaymentTypePayment");
            });

            modelBuilder.Entity<PaymentDistributed>(entity =>
            {
                entity.HasKey(e => e.IdPaymentDistributed);

                entity.HasIndex(e => e.ClientPhoneNumberIdPhoneNumber)
                    .HasName("IX_FK_ClientPhoneNumberPaymentDistributed");

                entity.HasIndex(e => e.PaymentIdPayment)
                    .HasName("IX_FK_PaymentPaymentDistributed");

                entity.Property(e => e.IdPaymentDistributed).HasColumnName("Id_PaymentDistributed");

                entity.Property(e => e.ClientPhoneNumberIdPhoneNumber).HasColumnName("ClientPhoneNumber_Id_PhoneNumber");

                entity.Property(e => e.PaymentIdPayment).HasColumnName("Payment_Id_Payment");

                entity.HasOne(d => d.ClientPhoneNumberIdPhoneNumberNavigation)
                    .WithMany(p => p.PaymentDistributed)
                    .HasForeignKey(d => d.ClientPhoneNumberIdPhoneNumber)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientPhoneNumberPaymentDistributed");

                entity.HasOne(d => d.PaymentIdPaymentNavigation)
                    .WithMany(p => p.PaymentDistributed)
                    .HasForeignKey(d => d.PaymentIdPayment)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PaymentPaymentDistributed");
            });

            modelBuilder.Entity<PaymentStatus>(entity =>
            {
                entity.HasKey(e => e.IdPaymentStatus);

                entity.Property(e => e.IdPaymentStatus).HasColumnName("Id_PaymentStatus");

                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<PaymentType>(entity =>
            {
                entity.HasKey(e => e.IdPaymentType);

                entity.Property(e => e.IdPaymentType).HasColumnName("Id_PaymentType");

                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<PriorityType>(entity =>
            {
                entity.HasKey(e => e.IdPriorityType);

                entity.Property(e => e.IdPriorityType).HasColumnName("Id_PriorityType");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ProfileGnk>(entity =>
            {
                entity.HasKey(e => e.IdProfileGnk);

                entity.ToTable("ProfileGNK");

                entity.Property(e => e.IdProfileGnk).HasColumnName("Id_ProfileGNK");

                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<Providers>(entity =>
            {
                entity.HasKey(e => e.IdProvider);

                entity.Property(e => e.IdProvider).HasColumnName("Id_Provider");

                entity.Property(e => e.ProvName)
                    .IsRequired()
                    .HasColumnName("Prov_Name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Requests>(entity =>
            {
                entity.HasKey(e => e.ReqId)
                    .HasName("PK_Requests_1");

                entity.Property(e => e.ReqId).HasColumnName("req_id");

                entity.Property(e => e.Ban)
                    .HasColumnName("ban")
                    .HasMaxLength(9)
                    .IsFixedLength();

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phone_number")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.RequestComments).HasColumnName("requestComments");

                entity.Property(e => e.RequestDate)
                    .HasColumnName("requestDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.RequestId).HasColumnName("requestId");

                entity.Property(e => e.RequestStatus).HasColumnName("requestStatus");

                entity.Property(e => e.RequestSubType)
                    .HasColumnName("requestSubType")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.RequestType)
                    .HasColumnName("requestType")
                    .HasMaxLength(10)
                    .IsFixedLength();
            });

            modelBuilder.Entity<Serv>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<ServiceName>(entity =>
            {
                entity.HasKey(e => e.IdService);

                entity.Property(e => e.ServiceName1)
                    .IsRequired()
                    .HasColumnName("serviceName")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Setting>(entity =>
            {
                entity.Property(e => e.Activ).HasColumnName("activ");

                entity.Property(e => e.Blok).HasColumnName("blok");

                entity.Property(e => e.Credinform).HasColumnName("credinform");

                entity.Property(e => e.DtBalanceInform).HasColumnType("datetime");

                entity.Property(e => e.Dtdop)
                    .HasColumnName("dtdop")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dtdopinv)
                    .HasColumnName("dtdopinv")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dtmaininv)
                    .HasColumnName("dtmaininv")
                    .HasColumnType("datetime");

                entity.Property(e => e.Infomun1)
                    .HasColumnName("infomun1")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Infonum2)
                    .HasColumnName("infonum2")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Ish).HasColumnName("ish");
            });

            modelBuilder.Entity<Sms>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("SMS");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SmsDirect).HasColumnName("SMS_DIRECT");

                entity.Property(e => e.SmsPhone)
                    .IsRequired()
                    .HasColumnName("SMS_PHONE")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.SmsText)
                    .HasColumnName("SMS_TEXT")
                    .HasColumnType("text");

                entity.Property(e => e.SmsTime)
                    .HasColumnName("SMS_TIME")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<SmsPattern>(entity =>
            {
                entity.HasKey(e => e.IdSmsPattern);

                entity.Property(e => e.IdSmsPattern).HasColumnName("Id_SmsPattern");

                entity.Property(e => e.ButtonText)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Text).IsRequired();
            });

            modelBuilder.Entity<SmsService>(entity =>
            {
                entity.ToTable("sms_service");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Num)
                    .HasColumnName("num")
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.Text).HasColumnName("text");
            });

            modelBuilder.Entity<Tariff>(entity =>
            {
                entity.HasKey(e => e.IdTariff);

                entity.Property(e => e.IdTariff).HasColumnName("Id_Tariff");

                entity.Property(e => e.AgentIdAgent).HasColumnName("Agent_Id_Agent");

                entity.Property(e => e.CallPacket).HasColumnName("call_packet");

                entity.Property(e => e.Category).HasColumnName("category");

                entity.Property(e => e.ChargeAmount).HasColumnName("chargeAmount");

                entity.Property(e => e.Code).IsRequired();

                entity.Property(e => e.DataPacket).HasColumnName("data_packet");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.EntityDesc).HasColumnName("entityDesc");

                entity.Property(e => e.LocalBee).HasColumnName("local_bee");

                entity.Property(e => e.LocalBeeCost).HasColumnName("local_bee_cost");

                entity.Property(e => e.LocalGprs).HasColumnName("local_gprs");

                entity.Property(e => e.LocalMnOut).HasColumnName("local_mn_out");

                entity.Property(e => e.LocalOut).HasColumnName("local_out");

                entity.Property(e => e.LocalOutCost).HasColumnName("local_out_cost");

                entity.Property(e => e.LocalSms).HasColumnName("local_sms");

                entity.Property(e => e.LocalSmsBee).HasColumnName("local_sms_bee");

                entity.Property(e => e.MgBee).HasColumnName("mg_bee");

                entity.Property(e => e.MgBeeCost).HasColumnName("mg_bee_cost");

                entity.Property(e => e.MgOut).HasColumnName("mg_out");

                entity.Property(e => e.MgOutCost).HasColumnName("mg_out_cost");

                entity.Property(e => e.MgSms).HasColumnName("mg_sms");

                entity.Property(e => e.MgSmsBee).HasColumnName("mg_sms_bee");

                entity.Property(e => e.MnGprs).HasColumnName("mn_gprs");

                entity.Property(e => e.MnIn).HasColumnName("mn_in");

                entity.Property(e => e.MnOut).HasColumnName("mn_out");

                entity.Property(e => e.MnSms).HasColumnName("mn_sms");

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.RcRate).HasColumnName("rcRate");

                entity.Property(e => e.RcRatePeriod).HasColumnName("rcRatePeriod");

                entity.Property(e => e.RoamBee).HasColumnName("roam_bee");

                entity.Property(e => e.RoamBeeCost).HasColumnName("roam_bee_cost");

                entity.Property(e => e.RoamGprs).HasColumnName("roam_gprs");

                entity.Property(e => e.RoamIn).HasColumnName("roam_in");

                entity.Property(e => e.RoamInCost).HasColumnName("roam_in_cost");

                entity.Property(e => e.RoamOut).HasColumnName("roam_out");

                entity.Property(e => e.RoamOutCost).HasColumnName("roam_out_cost");

                entity.Property(e => e.RoamSms).HasColumnName("roam_sms");

                entity.Property(e => e.SmsPacket).HasColumnName("sms_packet");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.HasOne(d => d.AgentIdAgentNavigation)
                    .WithMany(p => p.Tariff)
                    .HasForeignKey(d => d.AgentIdAgent)
                    .HasConstraintName("FK_AgentTariff");
            });

            modelBuilder.Entity<TariffOption>(entity =>
            {
                entity.HasKey(e => e.IdTariffOption);

                entity.Property(e => e.IdTariffOption).HasColumnName("Id_TariffOption");

                entity.Property(e => e.BaseFeatures).HasColumnName("baseFeatures");

                entity.Property(e => e.Category).HasColumnName("category");

                entity.Property(e => e.ChargeAmount)
                    .HasColumnName("chargeAmount")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.EntityDesc).HasColumnName("entityDesc");

                entity.Property(e => e.GroupName).HasColumnName("Group_Name");

                entity.Property(e => e.IdGroup).HasColumnName("Id_Group");

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.RcRate)
                    .HasColumnName("rcRate")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.RcRatePeriod).HasColumnName("rcRatePeriod");

                entity.Property(e => e.RcRatePeriodText).HasColumnName("rcRatePeriodText");

                entity.Property(e => e.SdbSize)
                    .HasColumnName("sdbSize")
                    .HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<TariffTariffOption>(entity =>
            {
                entity.HasKey(e => new { e.TariffIdTariff, e.TariffOptionIdTariffOption })
                    .IsClustered(false);

                entity.HasIndex(e => e.TariffOptionIdTariffOption)
                    .HasName("IX_FK_TariffTariffOption_TariffOption");

                entity.Property(e => e.TariffIdTariff).HasColumnName("Tariff_Id_Tariff");

                entity.Property(e => e.TariffOptionIdTariffOption).HasColumnName("TariffOption_Id_TariffOption");

                entity.HasOne(d => d.TariffIdTariffNavigation)
                    .WithMany(p => p.TariffTariffOption)
                    .HasForeignKey(d => d.TariffIdTariff)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TariffTariffOption_Tariff");

                entity.HasOne(d => d.TariffOptionIdTariffOptionNavigation)
                    .WithMany(p => p.TariffTariffOption)
                    .HasForeignKey(d => d.TariffOptionIdTariffOption)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TariffTariffOption_TariffOption");
            });

            modelBuilder.Entity<Task>(entity =>
            {
                entity.HasKey(e => e.IdTask);

                entity.Property(e => e.IdTask).HasColumnName("Id_Task");

                entity.Property(e => e.Comment).IsRequired();

                entity.Property(e => e.DateFinish).HasColumnType("datetime");

                entity.Property(e => e.DateStart).HasColumnType("datetime");

                entity.Property(e => e.DtDone).HasColumnType("datetime");

                entity.Property(e => e.TaskTypeIdTaskType).HasColumnName("TaskType_Id_TaskType");

                entity.Property(e => e.TriggerIdTrigger).HasColumnName("Trigger_Id_Trigger");

                entity.Property(e => e.UserCustomerIdUser).HasColumnName("UserCustomer_Id_User");

                entity.Property(e => e.UserPerformerIdUser).HasColumnName("UserPerformer_Id_User");

                entity.Property(e => e.UserRoleIdUserRole).HasColumnName("UserRole_Id_UserRole");

                entity.HasOne(d => d.PriorityNavigation)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.Priority)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Task_PriorityType");

                entity.HasOne(d => d.TargetPhoneNavigation)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.TargetPhone)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientPhoneNumberClientPhoneNumberConnectedToTask");

                entity.HasOne(d => d.TaskTypeIdTaskTypeNavigation)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.TaskTypeIdTaskType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TaskTypeTask");

                entity.HasOne(d => d.TriggerIdTriggerNavigation)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.TriggerIdTrigger)
                    .HasConstraintName("FK_TriggerTask");

                entity.HasOne(d => d.UserCustomerIdUserNavigation)
                    .WithMany(p => p.TaskUserCustomerIdUserNavigation)
                    .HasForeignKey(d => d.UserCustomerIdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserCustomerTask");

                entity.HasOne(d => d.UserPerformerIdUserNavigation)
                    .WithMany(p => p.TaskUserPerformerIdUserNavigation)
                    .HasForeignKey(d => d.UserPerformerIdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserPerformerTask");

                entity.HasOne(d => d.UserRoleIdUserRoleNavigation)
                    .WithMany(p => p.Task)
                    .HasForeignKey(d => d.UserRoleIdUserRole)
                    .HasConstraintName("FK_TaskUserRole");
            });

            modelBuilder.Entity<TaskByMessage>(entity =>
            {
                entity.HasKey(e => e.IdTaskByMessage);

                entity.Property(e => e.IdTaskByMessage).HasColumnName("Id_TaskByMessage");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.TaskTypeIdTaskType).HasColumnName("TaskType_Id_TaskType");

                entity.Property(e => e.UserPerformerIdUser).HasColumnName("UserPerformer_Id_User");

                entity.HasOne(d => d.PriorityNavigation)
                    .WithMany(p => p.TaskByMessage)
                    .HasForeignKey(d => d.Priority)
                    .HasConstraintName("FK_TaskByMessage_PriorityType");

                entity.HasOne(d => d.TaskTypeIdTaskTypeNavigation)
                    .WithMany(p => p.TaskByMessage)
                    .HasForeignKey(d => d.TaskTypeIdTaskType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TaskByMessage_TaskType");

                entity.HasOne(d => d.UserPerformerIdUserNavigation)
                    .WithMany(p => p.TaskByMessage)
                    .HasForeignKey(d => d.UserPerformerIdUser)
                    .HasConstraintName("FK_TaskByMessageUserPerformer");
            });

            modelBuilder.Entity<TaskType>(entity =>
            {
                entity.HasKey(e => e.IdTaskType);

                entity.Property(e => e.IdTaskType).HasColumnName("Id_TaskType");

                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<Trigger>(entity =>
            {
                entity.HasKey(e => e.IdTrigger);

                entity.Property(e => e.IdTrigger).HasColumnName("Id_Trigger");

                entity.Property(e => e.Code).IsRequired();

                entity.Property(e => e.Comment).IsRequired();

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.TriggerTypeIdTriggerType).HasColumnName("TriggerType_Id_TriggerType");

                entity.HasOne(d => d.TriggerTypeIdTriggerTypeNavigation)
                    .WithMany(p => p.Trigger)
                    .HasForeignKey(d => d.TriggerTypeIdTriggerType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TriggerTypeTrigger");
            });

            modelBuilder.Entity<TriggerType>(entity =>
            {
                entity.HasKey(e => e.IdTriggerType);

                entity.Property(e => e.IdTriggerType).HasColumnName("Id_TriggerType");

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.Pattern).IsRequired();
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.IdUser);

                entity.Property(e => e.IdUser).HasColumnName("Id_User");

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.PassHash).IsRequired();

                entity.Property(e => e.UserRoleIdUserRole).HasColumnName("UserRole_Id_UserRole");

                entity.HasOne(d => d.UserRoleIdUserRoleNavigation)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.UserRoleIdUserRole)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserRoleUser");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(e => e.IdUserRole);

                entity.Property(e => e.IdUserRole).HasColumnName("Id_UserRole");

                entity.Property(e => e.Name).IsRequired();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
