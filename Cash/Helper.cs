﻿using Cash.DataContext;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Helpers
{
    public class Helper
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();
        public static string GetClientFIO(Client client)
        {
            if (client == null)
            {
                return string.Empty;
            }
            string fio = string.Join(' ', client.SurName, client.Name, client.MiddleName);
            return string.IsNullOrWhiteSpace(fio) ? string.Empty : fio;
        }
        public static string GetClientFIO(string surName, string name, string middleName)
        {
            string fio = string.Join(' ', surName, name, middleName);
            return string.IsNullOrWhiteSpace(fio) ? string.Empty : fio;
        }

        public static void Log(string type, string message)
        {
            string text = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss.fff") + "|" + type + "| " + message;
            Console.WriteLine(text);
            switch(type)
            {
                case "T": logger.Trace(message); break;
                case "D": logger.Debug(message); break;
                case "I": logger.Info(message); break;
                case "W": logger.Warn(message); break;
                case "E": logger.Error(message); break;
                case "F": logger.Fatal(message); break;
            }
        }

    }
}
